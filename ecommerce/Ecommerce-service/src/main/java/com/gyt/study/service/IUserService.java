package com.gyt.study.service;

import com.gyt.study.pojo.dto.User;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName : IUserService
 * @Description : 用户service接口
 * @Author : gyt
 * @Date: 2020-04-27 09:10
 */
@Service
public interface IUserService {

    void login(User user, HttpServletRequest request);

    User selectByPrimaryKey(Long userId,HttpServletRequest request);

    User findAuthUserByUsername(String username);

    int registerUser(User user, HttpServletRequest request);
}
