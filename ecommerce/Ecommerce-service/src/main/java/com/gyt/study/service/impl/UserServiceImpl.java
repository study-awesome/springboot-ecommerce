package com.gyt.study.service.impl;

import com.gyt.study.mapper.UserMapper;
import com.gyt.study.pojo.dto.User;
import com.gyt.study.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName : UserService
 * @Description : 用户实现类
 * @Author : gyt
 * @Date: 2020-04-28 14:01
 */
@Service("userService")
public class UserServiceImpl implements IUserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public void login(User user, HttpServletRequest request) {

    }

    @Override
    public User selectByPrimaryKey(Long userId, HttpServletRequest request) {
        return null;
    }

    @Override
    public User findAuthUserByUsername(String username) {
        return userMapper.findAuthUserByUsername(username);
    }

    @Override
    public int registerUser(User user, HttpServletRequest request) {
        return userMapper.insertSelective(user);
    }
}
