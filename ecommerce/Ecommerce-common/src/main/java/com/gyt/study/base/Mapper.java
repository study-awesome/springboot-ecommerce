package com.gyt.study.base;

import tk.mybatis.mapper.common.BaseMapper;
import tk.mybatis.mapper.common.ConditionMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.special.InsertListMapper;

/**
 * Mapper
 * 定制 MyBatis Mapper插件接口，如需其他接口参考官方文档自行添加。
 *
 * @author ray
 * @date 2020/4/21
 */
public interface Mapper<T> extends
        BaseMapper<T>,
        ConditionMapper<T>,
        IdsMapper<T>,
        InsertListMapper<T> {


}
