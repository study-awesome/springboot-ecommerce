package com.gyt.study.base;

import lombok.Data;

import java.util.List;
import com.github.pagehelper.PageInfo;

/**
 * @ClassName : ResponsePage
 * @Description : 响应结果分页
 * @Author : gyt
 * @Date: 2020-04-27 14:39
 */
@Data
public class ResponsePage<T>{
    private Integer pageNum;
    private Integer pageSize;
    private Long total;
    private List<T> list;

    protected ResponsePage() {
    }

    public static <T> ResponsePage<T> getInstance(Integer pageNum, Integer pageSize, Long total, List<T> rows) {
        ResponsePage<T> responsePage = new ResponsePage<>();
        responsePage.setPageNum(pageNum);
        responsePage.setPageSize(pageSize);
        responsePage.setTotal(total);
        responsePage.setList(rows);
        return responsePage;
    }
    /**
     * 将PageHelper分页后的list转为分页信息
     */
    public static <T> ResponsePage<T> restPage(List<T> list) {
        ResponsePage<T> result = new ResponsePage<T>();
        PageInfo<T> pageInfo = new PageInfo<T>(list);
        result.setPageNum(pageInfo.getPageNum());
        result.setPageSize(pageInfo.getPageSize());
        result.setTotal(pageInfo.getTotal());
        result.setList(pageInfo.getList());
        return result;
    }



}
