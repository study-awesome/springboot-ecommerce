package com.gyt.study.base;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * BaseDO
 *
 * @author ray
 * @date 2020/4/6
 */
@Getter
@Setter
public class BaseDTO {

    //@Column
    private LocalDateTime createTime;

    //@Column
    private LocalDateTime updateTime;

}
