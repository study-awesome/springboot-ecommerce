package com.gyt.study.base;

import com.gyt.study.constants.Constants;

/**
 * @ClassName : BaseController
 * @Description : 基础控制类
 * @Author : gyt
 * @Date: 2020-04-26 16:07
 */
public class BaseController {

    // 返回成功 ,data值为null
    public ResponseData setResultSuccess() {
        return setResult(Constants.HTTP_RES_CODE_200, Constants.HTTP_RES_CODE_200_VALUE, null);
    }
    // 返回成功 ,data可传
    public ResponseData setResultSuccess(Object data) {
        return setResult(Constants.HTTP_RES_CODE_200, Constants.HTTP_RES_CODE_200_VALUE, data);
    }
    // 返回失败
    public ResponseData setResultError(String msg){
        return setResult(Constants.HTTP_RES_CODE_500,msg, null);
    }
    // 自定义返回结果
    public ResponseData setResult(Integer code, String message, Object data) {
        ResponseData responseBase = new ResponseData();
        if (data != null) {
            responseBase.setData(data);
        }
        return responseBase;
    }
}
