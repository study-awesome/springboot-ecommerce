package com.gyt.study.base;

/**
 * @ClassName : IErrorCode
 * @Description : 错误代码
 * @Author : gyt
 * @Date: 2020-04-27 11:27
 */
public interface IErrorCode {

    long getCode();

    String getMessage();
}
