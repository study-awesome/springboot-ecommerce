package com.gyt.study.base;
import org.apache.ibatis.exceptions.TooManyResultsException;
import tk.mybatis.mapper.entity.Condition;
import java.util.List;
import java.util.Set;

public interface BaseService<T> {

    //通过ID查找
    T findById(Long id);

    //通过多个ID查找//eg：ids -> “1,2,3,4”
    List<T> findByIds(Set<Long> ids);

    //根据条件查找
    List<T> findBy(Condition condition);
    T findOne(T entity);
    List<T> findBy(T entity);
    List<T> findBy(T entity,  int pageNum, int pageSize);

    //获取所有
    List<T> findAll();

    T insert(T entity);
    void insert(List<T> entityList);

    T update(T entity);

    void deleteById(Integer id);
    void deleteByIds(Set<Long> ids);

}
