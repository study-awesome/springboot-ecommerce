package com.gyt.study.mapper;

import com.gyt.study.base.Mapper;
import com.gyt.study.pojo.dto.User;
import org.apache.ibatis.annotations.Param;

/**
 * @ClassName : UserMapper
 * @Description : usermapper
 * @Author : gyt
 * @Date: 2020-05-07 18:00
 */
public interface UserMapper extends Mapper<User> {
    User findAuthUserByUsername(@Param("username") String username);

    int registerUser(User user);
}
