package com.gyt.study.evaluator;

import com.gyt.study.pojo.dto.SysPermission;
import com.gyt.study.pojo.dto.SysRole;
import com.gyt.study.pojo.dto.User;
import com.gyt.study.pojo.vo.JwtUser;
import com.gyt.study.service.SysPermissionService;
import com.gyt.study.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @ClassName : CustomPermissionEvaluator
 * @Description : 角色权限校验
 * @Author : gyt
 * @Date: 2020-05-12 16:44
 */
@Component
public class CustomPermissionEvaluator implements PermissionEvaluator {

    @Autowired
    private SysRoleService roleService;

    @Autowired
    private SysPermissionService permissionService;

    @Override
    public boolean hasPermission(Authentication authentication, Object targetUrl, Object targetPermission) {
        // 获得loadUserByUsername()方法的结果
        //JwtUser jwtUser = (JwtUser)authentication.getPrincipal();
        // 获得loadUserByUsername()中注入的角色
        String role = "";
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        // 遍历用户所有角色
        for(GrantedAuthority authority : authorities) {
            String roleCode = authority.getAuthority().replaceAll("ROLE_","");
            SysRole sysRole = new SysRole();
            sysRole.setRoleCode(roleCode);
            Long roleId = roleService.queryAll(sysRole).get(0).getRoleId();
            // 得到角色所有的权限
            SysPermission sysPermission = new SysPermission();
            sysPermission.setRoleId(roleId);
            List<SysPermission> permissionList = permissionService.queryAll(sysPermission);

            // 遍历permissionList
            for(SysPermission p : permissionList) {
                // 获取权限集
                String permissions = p.getPermission();
                // 如果访问的Url和权限用户符合的话，返回true
                if(targetUrl.equals(p.getUrl())
                                && permissions.equals(targetPermission)) {
                    return true;
                }
            }

        }

        return false;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable serializable, String s, Object o) {
        return false;
    }
}
