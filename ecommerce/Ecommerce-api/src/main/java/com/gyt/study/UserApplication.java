package com.gyt.study;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.request.RequestContextListener;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @ClassName : UserApplication
 * @Description : 用户启动类
 * @Author : gyt
 * @Date: 2020-05-07 15:26
 */
@SpringBootApplication
@EnableDiscoveryClient
// 扫描 mybatis mapper 包路径
@MapperScan(basePackages = "com.gyt.study.mapper")
// 组件扫描
public class UserApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }
    @Bean
    public RequestContextListener requestContextListener(){
        return new RequestContextListener();
    }

}
