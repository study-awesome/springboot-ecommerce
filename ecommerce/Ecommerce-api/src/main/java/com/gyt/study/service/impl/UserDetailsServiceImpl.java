package com.gyt.study.service.impl;

import com.gyt.study.pojo.dto.SysRole;
import com.gyt.study.pojo.dto.User;
import com.gyt.study.pojo.vo.JwtUser;
import com.gyt.study.service.IUserService;
import com.gyt.study.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName : UserServiceImpl  //类名
 * @Description : 用户实现类  //描述
 * @Author : gyt  //作者
 * @Date: 2020-04-26 15:42  //时间
 */
@Service( value = "userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private IUserService userService;

    @Autowired
    private SysRoleService sysRoleService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //查询用户信息
        User authUserPoJo = userService.findAuthUserByUsername(username);
        if(null==authUserPoJo){
            throw new UsernameNotFoundException("当前用户不存在");
        }
        // 角色信息
        SysRole sysRole = new SysRole();
        sysRole.setUserId(authUserPoJo.getId());
        List<SysRole> roleList = sysRoleService.queryAll(sysRole);
        if(roleList.isEmpty()){
            throw new UsernameNotFoundException("当前用户无角色");
        }
        return new JwtUser(authUserPoJo, roleList);
    }
}
