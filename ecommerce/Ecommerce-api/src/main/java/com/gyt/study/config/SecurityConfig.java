package com.gyt.study.config;

import com.gyt.study.filter.JWTAuthenticationFilter;
import com.gyt.study.filter.JWTAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.DigestUtils;

/**
 * @ClassName : SecurityConfig
 * @Description :
 * @Author : gyt
 * @Date: 2020-04-27 16:12
 */
@Configuration
@EnableWebSecurity// 这个注解必须加，开启Security
@EnableGlobalMethodSecurity(prePostEnabled = true)//保证post之前的注解可以使用
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    //先来这里认证一下
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        //校验用户
//        auth.userDetailsService( userDetailsService ).passwordEncoder( new PasswordEncoder() {
//            //对密码进行加密
//            @Override
//            public String encode(CharSequence charSequence) {
//                System.out.println(charSequence.toString());
//                return DigestUtils.md5DigestAsHex(charSequence.toString().getBytes());
//            }
//            //对密码进行判断匹配
//            @Override
//            public boolean matches(CharSequence charSequence, String s) {
//                String encode = DigestUtils.md5DigestAsHex(charSequence.toString().getBytes());
//                boolean res = s.equals( encode );
//                return res;
//            }
//        } );
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoderBean());
    }

    //拦截在这配
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 取消跨站请求伪造防护
        http.csrf().disable()
                        //因为使用JWT，所以不需要HttpSession
                        .sessionManagement().sessionCreationPolicy( SessionCreationPolicy.STATELESS).and()
                        .authorizeRequests()
                        //OPTIONS请求全部放行
                        .antMatchers( HttpMethod.OPTIONS, "/**").permitAll()
                        //登录接口放行
                        .antMatchers("/auth/login").permitAll()
                        .antMatchers("/v1/auth/register").permitAll()
                        .antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources", "/configuration/security", "/doc.html", "/swagger-ui.html", "/webjars/**","/swagger-resources/configuration/ui","/swagger-resources/configuration/security").permitAll()
                        //其他接口全部接受验证
                        .anyRequest().authenticated()
                        .and()
                        // 开启跨域
                        .cors()
                        .and()
                        //添加自定义权限过滤器
                        //添加JWT过滤器 除/login其它请求都需经过此过滤器
                        .addFilter(new JWTAuthorizationFilter(authenticationManager()));

        //使用自定义的 Token过滤器 验证请求的Token是否合法
        http.addFilterBefore(new JWTAuthenticationFilter(authenticationManager()), UsernamePasswordAuthenticationFilter.class);
        http.headers().cacheControl();
    }

    @Bean
    public PasswordEncoder passwordEncoderBean() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
