package com.gyt.study.interfaces;

import org.redisson.api.RLock;

import java.util.concurrent.TimeUnit;

/**
 * @ClassName : DistributedLocker
 * @Description : redisson操作锁
 * @Author : gyt
 * @Date: 2020-05-20 16:02
 */
public interface DistributedLocker {

    RLock lock(String lockKey);

    RLock lock(String lockKey, long timeout);

    RLock lock(String lockKey, TimeUnit unit, long timeout);

    boolean tryLock(String lockKey, TimeUnit unit, long waitTime, long leaseTime);

    void unlock(String lockKey);

    void unlock(RLock lock);

}

