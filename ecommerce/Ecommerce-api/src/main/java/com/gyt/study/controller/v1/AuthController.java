package com.gyt.study.controller.v1;

import com.gyt.study.base.ResponseData;
import com.gyt.study.pojo.bo.RegistLoginUserBO;
import com.gyt.study.pojo.dto.User;
import com.gyt.study.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName : UserApiService  //类名
 * @Description : 用户API  //描述
 * @Author : gyt  //作者
 * @Date: 2020-04-26 15:33  //时间
 */
@RequestMapping("/v1/auth")
@RestController
@Api(value = "权限", tags = {"权限认证相关接口"})
public class AuthController {

    @Autowired
    private IUserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    /**
     * 用户注册接口
     * @param userBO
     * @return
     */
    @PostMapping("/register")
    @ApiOperation(value = "用户注册", notes = "用户注册", httpMethod = "POST")
    public ResponseData register (@Validated @RequestBody RegistLoginUserBO userBO, HttpServletRequest request){
        // 0. 判断用户名和密码不能为空
        if (StringUtils.isBlank(userBO.getPassword()) || StringUtils.isBlank(userBO.getUsername())) {
            return ResponseData.failed("用户名或密码不能为空...");
        }

        User registerUser = User.builder().build();
        registerUser.setUsername(userBO.getUsername());
        registerUser.setPassword(bCryptPasswordEncoder.encode(userBO.getPassword()));
        registerUser.setSex("M");
        userService.registerUser(registerUser,request);
        return ResponseData.success();
    }
    @PostMapping("/test")
    @ApiOperation(value = "用户注册", notes = "用户注册", httpMethod = "POST")
    @PreAuthorize("hasPermission('/admin','write')")
    public ResponseData test (@RequestBody RegistLoginUserBO userBO, HttpServletRequest request){
        System.out.println("11111111");
        return ResponseData.success();
    }

}
