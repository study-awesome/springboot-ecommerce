package com.gyt.study.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @ClassName : Swagger2
 * @Description : swagger配置 为文档restful api服务
 * @Author : gyt
 * @Date: 2020-04-28 09:19
 */
@Configuration
@EnableSwagger2
@EnableWebMvc
@ComponentScan(basePackages = {"com.gyt.study.controller.v1"})
public class Swagger2 {
    // http://[ip]:[port]/[prjName]/swagger-ui.html
    // http://[ip]:[port]/[prjName]/doc.html

    /**
     * swagger2 的配置项，可以配置swagger2的一些基本内容以及扫描的api
     * @return
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                        .apiInfo(apiInfo())
                        .select()
                        .apis(RequestHandlerSelectors.basePackage("com.gyt.study.controller.v1"))
                        .paths(PathSelectors.any())
                        .build();
    }

    /**
     * 构建文档api的基本信息
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                        .title("api接口文档")		// 设置标题
                        .contact(new Contact("Seek Plan", "", "guiyuting941117@163.com"))
                        .description("电影预告文档的描述信息")
                        .version("1.0.1")
                        .termsOfServiceUrl("www.qq.com")
                        .build();
    }
}
