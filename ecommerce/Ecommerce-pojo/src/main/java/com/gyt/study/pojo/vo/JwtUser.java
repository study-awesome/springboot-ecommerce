package com.gyt.study.pojo.vo;

import com.gyt.study.pojo.dto.SysRole;
import com.gyt.study.pojo.dto.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @ClassName : JwtUser
 * @Description :
 * @Author : gyt
 * @Date: 2020-04-27 15:33
 */
@Getter
@Setter
public class JwtUser implements UserDetails {

    private Long id;
    private String username;
    private String password;
    private List<SimpleGrantedAuthority> authorities = new ArrayList<>();

    public JwtUser(){

    }

    public JwtUser(User user, List<SysRole> roles) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.password = user.getPassword();
        for (SysRole role : roles) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getRoleCode()));
            System.out.println("Role:" + role.getRoleCode());
        }
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    /**
     * 账户是否过期
     *
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {

        return true;
    }

    /**
     * 是否禁用
     *
     * @return
     */
    @Override
    public boolean isAccountNonLocked() {

        return true;
    }

    /**
     * 密码是否过期
     *
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {

        return true;
    }

    /**
     * 是否启用
     *
     * @return
     */
    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String toString() {
        return "JwtUser{" +
                        "id=" + id +
                        ", username='" + username + '\'' +
                        ", password='" + password + '\'' +
                        ", authorities=" + authorities +
                        '}';
    }

}
