package com.gyt.study.pojo.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

/**
 * @ClassName : RegistLoginUserBO
 * @Description :
 * @Author : gyt
 * @Date: 2020-04-28 10:02
 */
@ApiModel(value="用户对象", description="从客户端，由用户传入的数据封装在此entity中")
public class RegistLoginUserBO {

    @ApiModelProperty(value="用户名",name="username",example="jack", required=true)
    @NotNull(message = "用户名不能为空")
    private String username;

    @ApiModelProperty(value="密码",name="password",example="123456", required=true)
    @NotNull(message = "密码不能为空")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
