package com.gyt.study.pojo.dto;

import lombok.Data;
                /**
 * (SysRole)实体类
 *
 * @author guiyuting
 * @since 2020-05-13 17:26:57
 */
@Data 
public class SysRole {
    
        private Long roleId;
    
        private String roleCode;
    
        private String roleName;
    
        private Long userId;


}
