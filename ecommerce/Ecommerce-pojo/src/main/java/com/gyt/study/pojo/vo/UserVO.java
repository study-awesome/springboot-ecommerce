package com.gyt.study.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @ClassName : UserVO
 * @Description :
 * @Author : gyt
 * @Date: 2020-04-28 10:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserVO {
    private String id;
    private String username;
    private String nickname;
    private Integer sex;
    private String birthday;
    private String faceImage;
    private Integer isCertified;
    private Date registTime;

    // 用户唯一令牌
    private String userUniqueToken;
}
