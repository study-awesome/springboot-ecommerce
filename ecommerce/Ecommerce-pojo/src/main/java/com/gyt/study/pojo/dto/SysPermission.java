package com.gyt.study.pojo.dto;

import lombok.Data;
                /**
 * (SysPermission)实体类
 *
 * @author guiyuting
 * @since 2020-05-13 17:28:57
 */
@Data 
public class SysPermission {
    
        private Long permissionId;
    
        private String url;
    
        private Long roleId;
    
        private String permission;

}