package com.gyt.study.pojo.dto;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
/**
 * @ClassName : User
 * @Description : 用户实体类
 * @Author : gyt
 * @Date: 2020-04-27 09:20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "sys_user")
public class User {

    @Id
    private Long id;

    private String username;

    private String password;

    private String sex;
}
